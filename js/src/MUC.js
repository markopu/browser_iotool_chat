XmppChat.MUC = (function(xc) {
    'use strict';

    var room_subjects = {};

    function registerHangUpConference(room) {
	var media_page = $('#muc_media_conference');
	var hang_up = media_page.find('.muc_hang_up');
	var room_id = '#' + xc.Utils.jidToId(room);
	
	hang_up.off('click');
	hang_up.on('click', function() {
	    xc.leaveConferenceRoom();
	    xc.goBackInStack();

	    // Last one alive close the room :)
	    xc.queryConferenceOccupants(function(occupants) {
		if (!occupants) {
		    xc.MUC.changeSubject(room, '');
		}
	    });
	});
    }
    
    /**
     * Query basic information about multi user chat room
     */
    xc.queryRoomInfo = function(room, callback) {
	var iq = $iq({to: room, type: 'get'}).c('query', {
	    xmlns: 'http://jabber.org/protocol/disco#info'
	});

	xc.connection.sendIQ(iq, function(s) {
	    callback(s);
	}, function(e) {
	    console.log(e);
	});
    };

    xc.getRemoteRoomName = function(room, callback) {
	xc.queryRoomInfo(room, function(info) {
	    callback($(info).find('identity').attr('name'));
	});
    };

    // Do things on connection
    xc.onConnectedMuc = function() {
	xc.muc_affiliations = {};
	xc.MUC.loadBookmarks();
	
	xc.connection.addHandler(function(msg) {
	    console.log(msg);
	    
	    var from = $(msg).attr('from');
	    var body = $(msg).find('x').attr('reason');
	    var room_id = $(msg).find('x').attr('jid');
	    var password = $(msg).find('x').attr('password');

	    console.log(room_id);

	    xc.getRemoteRoomName(room_id, function(name) {
		console.log(name);
		var s = $('<div class="muc_invitation">You have been invited to the conference room: ' +
			  name + '. ' + body +
			  '<div class="conference_join_dialog">' +
			  '<button class="muc_acc_invitation">Join</button>' +
			  '<button class="muc_cancel_invitation" >Cancel</button>' +
			  '</div></div>');

		s.find('.muc_acc_invitation').on('click', function() {
		    console.log('Accept', room_id);
		    var id = xc.Utils.jidToId(room_id);
		    var nick = xc.connection.authcid;

		    xc.joinRoom(room_id, nick);
		    xc.saveBookmark(id, room_id, name, nick, true);
		    createRoomEntry(id, room_id, name);
		    xc.createChatRoomUI(id, room_id, name);
		    xc.navigateToPage('#' + id);

		    s.remove();
		});

		s.find('.muc_cancel_invitation').on('click', function() {
		    console.log('Cancel', room_id);
		    // Hide this dialog
		    s.remove();
		});

		xc.appendMessage(from, s, body);
		$('.conference_join_dialog').enhanceWithin();
	    });

	    return true;
	}, 'jabber:x:conference');
    };

    function errorHandler(error) {
	console.error(error);
    }


    xc.getRoomName = function(room) {
	var id = xc.Utils.jidToId(room);
	return xc.room_bookmarks[id].alias;
    };

    xc.sendInvitation = function(jid, room, password, reason) {
	var m = $msg({to: jid}).c('x', {
	    xmlns: 'jabber:x:conference',
	    jid: room,
	    password: password,
	    reason: reason
	});

	console.log(m.tree());

	xc.connection.send(m, function(s) {
	    console.log(s);
	}, function(e) {
	    console.log(e);
	});
    };

    xc.leaveRoom = function(room) {
	// Leave room
	var nick = xc.connection.authcid;
	xc.connection.muc.leave(room, nick);
	
	var id = xc.Utils.jidToId(room);
	
	xc.removeBookmark(room);
	$('#room-entry-' + id).remove();
	$('#' + id).remove();
    };

    function sendInvitations(room, reason, invited) {
	_.forEach(invited, function(jid) {
	    xc.sendInvitation(jid, room, '', reason);
	});
    }

    function destroyRoom(room) {
	var iq = $iq({to: room, type: 'set'}).c('query', {
	    xmlns: 'http://jabber.org/protocol/muc#owner'
	}).c('destroy').c('reason').t('Owner left room.');

	xc.connection.sendIQ(iq, function(s) {
	    console.log(s);
	}, function(e) {
	    console.log(e);
	});
    }

    function createRoomEntry(id, room, name) {
	var entry = '<div id="room-entry-' + id + '" class="muc_room_entry" data-room="' +
	    room + '">' +
	    '<div class="muc_entry_name">' + name + '</div>' +
	    '<div class="muc_remove_room">' +
	    '<i class="fa fa-times" aria-hidden="true"></i></div>' +
	    '<div class="muc_unread">0</div>' +
	    '</div>';
	var e = $(entry);

	e.find('.muc_remove_room').on('click', function(e) {
	    e.stopPropagation();

	    navigator.notification.confirm(
		'Are you sure you want to leave this room?' +
		    ' You will have to receive another invitation to join again.',
		function(index) {
		    if (index === 1) {
			xc.leaveRoom(id);
			// destroy room if owner
			destroyRoom(room);
		    }
		},
		'Leave Room',
		['Leave', 'Cancel']
	    );

	});

	e.find('.muc_unread').hide();
	e.on('click', function() {
	    var room_jid = $(this).data('room');
	    var room_id = xc.Utils.jidToId(room_jid);
	    var id = '#' + xc.Utils.jidToId(room_jid);

	    function onNavigate() {
		var container = $(id).find('.muc_messages_container');
		xc.adjustMessageContainerHeight(id);
		container.scrollTop(container.prop('scrollHeight'));
	    }

	    if ($(id).length) {
		xc.Utils.navigateToPage(id, onNavigate);
	    } else {
		xc.createChatRoomUI(room_id, room_jid, name);
		xc.Utils.navigateToPage(id, onNavigate);
	    }

	    // Reset unread counter to zero and hide it
	    var counter = $(this).find('.muc_unread');
	    counter.hide();
	    counter.text('0');
	});
	
	$('#rooms_area').append(e);
    }
    
    xc.sendRoomConfiguration = function(room_id, config, success, error) {
	var iq = $iq({to: room_id, type: 'set'}).c('query', {
	    xmlns: 'http://jabber.org/protocol/muc#owner'
	}).c('x', {
	    xmlns: 'jabber:x:data',
	    type: 'submit'
	}).c('field', {var: 'FORM_TYPE'}).c('value').t(
	    'http://jabber.org/protocol/muc#roomconfig'
	).up().up();

	for (var entry in config) {
	    iq.c('field', {var: entry});
	    iq.c('value').t(config[entry]).up().up();
	}

	xc.connection.sendIQ(iq, success, error);
    };

    function loadSettings(id, room_jid, name) {
	xc.connection.muc.configure(room_jid, function(xml) {
	    console.log(xml);
	    xc.Utils.navigateToPage('#muc_settings');
	    // Need to get current: name, description, persistent, public
	    var config_name = $(xml).find('[var="muc#roomconfig_roomname"]').children().text();
	    var description = $(xml).find('[var="muc#roomconfig_roomdesc"]').children().text();
	    var persistent = $(xml).find('[var="muc#roomconfig_persistentroom"]').children().text();
	    var public_room = $(xml).find('[var="muc#roomconfig_publicroom"]').children().text();
	    	    
	    console.log('config_name', config_name);
	    console.log('description', description);
	    console.log('persistent', persistent);
	    console.log('public_room', public_room);

	    // Load values in to configuration page
	    $('#muc_settings_title').text(name + ' settings');
	    $('#muc_settings_room_name').val(config_name);
	    $('#muc_settings_room_desc').val(description);

	    if (persistent === '1') {
		$('#muc_settings_persistent').prop('checked', true);
	    } else {
		$('#muc_settings_persistent').prop('checked', false);
	    }

	    $('#muc_settings_persistent').checkboxradio('refresh');

	    if (public_room === '1') {
		$('#muc_settings_public').prop('checked', true);
	    } else {
		$('#muc_settings_public').prop('checked', false);
	    }

	    $('#muc_settings_public').checkboxradio('refresh');


	    $('#muc_settings_save').off('click');
	    $('#muc_settings_cancel').off('click');
	    $('#muc_back_muc_settings').off('click');

	    $('#muc_settings_save').on('click', function() {
		var new_name = $('#muc_settings_room_name').val();
		var new_desc = $('#muc_settings_room_desc').val();
		var new_per = $('#muc_settings_persistent').prop('checked') === true ? '1' : '0';
		var new_pub = $('#muc_settings_public').prop('checked') === true ? '1' : '0';

		console.log('Got values');
		xc.sendRoomConfiguration(room_jid, {
		    'muc#roomconfig_roomname': new_name,
		    'muc#roomconfig_roomdesc': new_desc,
		    'muc#roomconfig_persistentroom': new_per,
		    'muc#roomconfig_publicroom': new_pub,
		    'muc#roomconfig_whois': 'moderators',
		    'muc#roomconfig_changesubject': '1'
		}, function(s) {
		    console.log(s);
		}, function(e) {
		    console.log(e);
		});

		xc.Utils.navigateToPage('#' + id);
	    });

	    $('#muc_settings_cancel').on('click', function() {
		xc.Utils.navigateToPage('#' + id);
	    });

	    $('#back_muc_settings').on('click', function() {
		xc.Utils.navigateToPage('#' + id);
	    });
	});
    }

    xc.createChatRoomUI = function(id, room_jid, name) {
	var settings_btn;

	// Display | ommit settings button if user is not an owner
	if (xc.muc_affiliations[room_jid] === 'owner') {
	    settings_btn = '<a class="settings_muc" data-role="button"' +
		'data-icon="gear" data-iconpos="notext"></a>';
	} else {
	    settings_btn = '';
	}
	
	var ui = $('<div data-role="page" id="' + id + '">' +
		   '<div data-role="header" class="muc_header">' +
		   '<a class="back_muc" data-role="button"' +
		   'data-icon="carat-l" data-iconpos="notext"></a>' +
		   '<h3 class="muc_header_title">' + name + '</h3>' +
		   '<div class="ui-btn-right muc_room_actions">' +
		   settings_btn +
		   '<a class="muc_media_conference" data-role="button"' +
		   'data-icon="video" data-iconpos="notext"></a>' +
		   '<a class="invite_additional_muc" data-role="button"' +
		   'data-icon="plus" data-iconpos="notext"></a>' +
		   '</div>' +
		   '</div>' +
		   '<div data-role="main" class="muc_main">' +
		   '<div class="muc_notification_area"></div>' +
		   '<div class="muc_messages_container">' +
		   '</div>' +
		   '<div class="muc_message_input">' +
		   '<div class="muc_input_overlay"></div>' +
		   '<textarea class="muc_textarea" data-role="none"></textarea>' +
		   '<a class="send_muc_msg" data-role="button"' +
		   'data-icon="arrow-r" data-iconpos="notext"></a>' +
		   '</div>' +
		   '</div>' +
		   '</div>');

	ui.find('.back_muc').on('click', function() {
	    xc.Utils.navigateToPage('#chats');
	});

	ui.find('.settings_muc').on('click', function() {
	    loadSettings(id, room_jid, name);
	});

	ui.find('.muc_media_conference').on('click', function() {
	    if (!room_subjects[room_jid]) {
		xc.muWebRTC.createConferenceRoom(room_jid);
		xc.back_stack.push(xc.getPageID());
		registerHangUpConference(room_jid);
		xc.Utils.navigateToPage('#muc_media_conference');
	    }
	});

	ui.find('.invite_additional_muc').on('click', function() {
	    $('#confirm_selected_add_invitations').off('click');
	    $('#back_additional_invitations').off('click');
	    
	    xc.focused_room_jid = room_jid;
	    xc.focused_room_id = xc.Utils.jidToId(room_jid);
	    
	    xc.navigateToPage('#invite_users', function() {
		xc.UI.generateUserSearchList('#add_invitation_search_results', xc.user_roster, function(jid, id) {
		    $('#' + id).toggleClass('invitation_selected');
		});
	    });

	    xc.bindButton('#confirm_selected_add_invitations', function() {
		console.log('Sending invitations');
		var invited = [];
		
		$('#add_invitation_search_results').children().each(function() {
		    if ($(this).hasClass('invitation_selected')) {
			invited.push($(this).data('jid'));
		    }
		});

		console.log(invited);
		sendInvitations(xc.focused_room_jid, '', invited);

		$('#add_invitation_search_results').empty();
		$('#add_invitation_search').val('');
		
		xc.Utils.navigateToPage('#' + xc.focused_room_id);

	    });

	    xc.UI.bindSearchInputChange('#add_invitation_search', function() {
		xc.UI.filterSearchResults('#add_invitation_search_results', $(this).val());
	    });

	    xc.bindButton('#back_additional_invitations', function() {
		xc.Utils.navigateToPage('#' + xc.focused_room_id);
	    });
	});

	function sendMessage(room, message) {
	    message.replace(/\v+/g, '');

	    if (message !== '') {
		xc.connection.muc.groupchat(room, message);
		ui.find('.muc_textarea').val('');
	    }
	}

	ui.find('.send_muc_msg').on('click', function() {
	    var message = ui.find('.muc_textarea').val();
	    sendMessage(room_jid, message);
	});

	ui.find('.muc_textarea').bind('keypress', function(e) {
	    var message = ui.find('.muc_textarea').val();
	    var code = e.keyCode || e.which;

	    if (code === 13) {
		e.preventDefault();
		sendMessage(room_jid, message);
	    }
	});
	
	$('body').append(ui);
    };
    
    xc.initMUC = function() {
	xc.UI.bindCreateRoomPopupBtn(function() {
	    xc.Utils.navigateToPage('#create_room');
	});

	xc.UI.bindAddInvitationBtn(function() {
	    xc.Utils.navigateToPage('#select_invitations', function() {
		console.log('Generating user search list');
		xc.UI.generateUserSearchList('#invitation_search_results', xc.user_roster, function(jid, id) {
		    console.log(jid, id);
		    // Toggle selected class
		    $('#' + id).toggleClass('invitation_selected');
		});
	    });
	});

	xc.bindButton('#confirm_selected_invitations', function() {
	    // Copy selected entries to #room_invitations
	    $('#room_invitations').empty();
	    
	    $('#invitation_search_results').children().each(function() {
		if ($(this).hasClass('invitation_selected')) {
		    var jid = $(this).data('jid');
		    var name = $(this).eq(0).text();
		    console.log(name, jid);

		    var container = '<div class="room_invitations_entry" data-jid="' +
			jid + '">' +
			name +
			'<i class="fa fa-times inv_remove" aria-hidden="true"></i>' +
			'</div>';

		    $('#room_invitations').append(container);
		}
	    });

	    $('.inv_remove').on('click', function() {
		$(this).parent().remove();
	    });

	    xc.Utils.changePage('#create_room');
	});

	xc.UI.bindSearchInputChange('#invitation_search', function() {
	    xc.UI.filterSearchResults('#invitation_search_results', $(this).val());
	});

	xc.UI.bindStandardBackBtn('#back_select_invitations', function() {
	    xc.Utils.navigateToPage('#create_room');
	});

	xc.UI.bindStandardBackBtn('#back_create_room', function() {
	    xc.Utils.navigateToPage('#chats');
	    $('#room_invitations').empty();
	});


	xc.testSendConfig = function(room_id, config) {
	    xc.sendRoomConfiguration(room_id, config);
	};

	xc.UI.bindButton('#create_room_dialog_btn', function() {
	    // Generate random room identifier
	    var room_name = $('#room_name').val();
	    var room_id = xc.Utils.generateRandomString(20) + '@' + xc.CONFERENCE_SERVER;
	    var nickname = xc.connection.authcid;
	    var id = xc.Utils.jidToId(room_id);

	    xc.joinRoom(room_id, nickname);
	    
	    xc.connection.muc.configure(room_id, function(h) {
		console.log(h);

		xc.sendRoomConfiguration(room_id, {
		    'muc#roomconfig_roomname': room_name,
		    'muc#roomconfig_whois': 'moderators',
		    'muc#roomconfig_persistentroom': '1',
		    'muc#roomconfig_changesubject': '1'
		}, function(s) {
		    console.log(s);
		    console.log('Room created and configured');

		    // Begin sending invitations
		    var invited = [];
		    $('#room_invitations').children().each(function() {
			invited.push($(this).data('jid'));
		    });
		    var reason = $('#invite_reason').val();
		    sendInvitations(room_id, reason, invited);

		    xc.createChatRoomUI(id, room_id, room_name);
		    xc.Utils.navigateToPage('#' + id);
		}, errorHandler);

	    }, function(e) {
		console.log(e);
	    });

	    xc.saveBookmark(id, room_id, room_name, nickname, true);
	    createRoomEntry(id, room_id, room_name);
	});

	xc.UI.bindButton('#cancel_create_room_dialog_btn', function() {
	    xc.Utils.navigateToPage('#chats');
	    $('#room_invitations').empty();
	});
    };

    xc.createRoom = function(room, nickname) {
	var pres = $pres({to: room + '/' + nickname}).c('x', {
	    xmlns: 'http://jabber.org/protocol/muc'
	});

	xc.connection.send(pres);
    };

    /**
     * @param {[object]} container 
     */
    function jsonToXml(json) {
	if (json === {}) {
	    return '<items></items>';
	} else {
	    var s = '<items>';
	    for (var key in json) {
		var o = json[key];
		s += '<item id="' + key + '"><room>' + o.room + '</room>' +
		    '<alias>' + o.alias + '</alias>' +
		    '<nick>' + o.nick + '</nick>' +
		    '<autojoin>' + o.autojoin + '</autojoin>' +
		    '</item>';
	    }

	    s += '</items>';

	    return s;
	}
    }

    function xmlToJson(xml) {
	var container = {};
	if (typeof xml === 'undefined') {
	    return container;
	} else {
	    var items = $(xml).children();
	    items.each(function() {
		var room = $(this).find('room').text();
		var alias = $(this).find('alias').text();
		var nick = $(this).find('nick').text();
		var autojoin = $(this).find('autojoin').text();

		var id = $(this).attr('id');
		container[id] = {
		    room: room,
		    alias: alias,
		    nick: nick,
		    autojoin: autojoin
		};
	    });

	    return container;
	}
    }

    function storeXmlContainer(container) {
	xc.connection.private.set('muc', 'bookmarks', container, function(s) {
	}, function(e) {
	});
    }

    xc.getBookmarks = function(callback) {
	xc.connection.private.get('muc', 'bookmarks', function(xml) {
	    callback(xml);
	}, function (e) {
	});
    };

    // If passed parameter is undefined don't change the value
    xc.updateBookmark = function(id, alias, nick, autojoin) {
	xc.getBookmarks(function(xml) {
	    var json_bookmarks = xmlToJson(xml);

	    if (alias) {
		json_bookmarks[id].alias = alias;
	    }

	    if (nick) {
		json_bookmarks[id].nick = nick;
	    }

	    if (autojoin) {
		json_bookmarks[id].autojoin = nick;
	    }

	    var container = jsonToXml(json_bookmarks);
	    storeXmlContainer(container);
	});
    };
    
    xc.saveBookmark = function(id, room, alias, nick, autojoin) {
	if (arguments.length !== 5) {
	    console.error('Incorrect number of parameters.');
	    return false;
	}
	
	var bookmark = {
	    room: room,
	    alias: alias,
	    nick: nick,
	    autojoin: autojoin
	};

	xc.getBookmarks(function(xml) {
	    var json_bookmarks = xmlToJson(xml);
	    json_bookmarks[id] = bookmark;

	    var container = jsonToXml(json_bookmarks);
	    storeXmlContainer(container);
	});
    };
    
    xc.removeBookmark = function(room) {
	var id = xc.Utils.jidToId(room);
	
	xc.getBookmarks(function(xml) {
	    var json_bookmarks = xmlToJson(xml);
	    delete json_bookmarks[id];

	    var container = jsonToXml(json_bookmarks);
	    storeXmlContainer(container);	    
	});
    };

    
    function syncConfigBookmark(room_jid) {
	xc.queryRoomInfo(room_jid, function(xml) {
	    var new_alias = $(xml).find('identity').attr('name');

	    var id = xc.Utils.jidToId(room_jid);
	    // Update current ui entreies
	    $('#room-entry-' + id).find('.muc_entry_name').text(new_alias);
	    $('#' + id).find('.muc_header_title').text(new_alias);

	    xc.updateBookmark(id, new_alias);
	});
    }

    xc.loadBookmarks = function() {
	xc.getBookmarks(function(xml) {
	    var json_bookmarks = xmlToJson(xml);

	    xc.room_bookmarks = json_bookmarks;

	    for (var key in json_bookmarks) {
		var room = json_bookmarks[key].room;
		var alias = json_bookmarks[key].alias;
		var nick = json_bookmarks[key].nick;
		var autojoin = json_bookmarks[key].autojoin;
		
		createRoomEntry(key, room, alias);

		// Sync bookmarks with configurations
		syncConfigBookmark(room);

		if (autojoin === "true") {
		    xc.joinRoom(room, nick);
		    console.log('Auto-joining room:', room);
		}
	    }
	});
    };

    function getNickname(from) {
	var s_index = from.indexOf('/');
	return from.substring(s_index + 1);
    }

    function getRoom(from) {
	var s_index = from.indexOf('/');
	var s;

	if (s_index === -1) {
	    s = from;
	} else {
	    s = from.substring(0, s_index);
	}

	return s;
    }

    function onRoomMessage(message) {
	console.log(message);
	var from = $(message).attr('from');
	var room = getRoom(from);
	var nickname = getNickname(from);
	var body = $(message).find('body').text();
	var timestamp = xc.Utils.getFormatedTime(xc.Utils.getTime());
	var room_id = '#' + xc.Utils.jidToId(room);
	var delay = $(message).find('delay');
	var data = $(message).find('data');
	var subject = $(message).find('subject');

	// Check for configuration updates
	if ($(message).find('status').length) {
	    // 104 indicates configuration change
	    if ($(message).find('status').attr('code') === '104') {
		syncConfigBookmark(Strophe.getBareJidFromJid($(message).attr('from')));
	    }
	}
	
	function appendMessage(room_id, timestamp, nickname, body) {
	    var s = $('<div class="muc_message">' +
		      '<span class="muc_timestamp">' +
		      timestamp +
		      '</span><span class="muc_sender"> ' +
		      nickname +
		      ' </span><p>' +
		      body +
		      '</p></div>');
	    var container = $(room_id).find('.muc_messages_container');

	    if (nickname === xc.connection.authcid) {
		s.addClass('muc_own_message');
	    }

	    container.append(s);

	    if ('#' + xc.getPageID() === room_id) {
		container.scrollTop(container.prop('scrollHeight'));
	    }
	}


	if (data.length) {
	    // MC - media conference type used to indicate
	    // request for room level conference
	    if (data.attr('type') === 'MC') {
		console.log(nickname, 'requested media conference in room', data.text());
		var s = $('<div class="muc_message"><p>' +
			  'Conference request' +
			  '</p></div>');

		$(room_id).find('.muc_messages_container').append(s);
	    }
	} else if (body.length) {
	    // Check if ui for room is already there
	    if ($(room_id).length) {
		appendMessage(room_id, timestamp, nickname, body);
	    } else {
		var name = xc.getRoomName(room);
		xc.createChatRoomUI(xc.Utils.jidToId(room), room, name);
		appendMessage(room_id, timestamp, nickname, body);
	    }

	    // Check if user is currently in the room
	    // If not increment unread counter and fire notification
	    if (xc.getPageID() !== xc.Utils.jidToId(room) && !delay.length) {
		var room_entry = $('#room-entry-' + xc.Utils.jidToId(room));
		var counter = room_entry.find('.muc_unread');
		var unread = parseInt(counter.text()) + 1;
		counter.text(unread);
		counter.show();
	    }
	} else if (subject.length) {
	    $(window).resize(function() {
		xc.adjustMessageContainerHeight(room_id);

		if ('#' + xc.getPageID() === room_id) {
		    container.scrollTop(container.prop('scrollHeight'));
		}
	    });
	    
	    var button = $(room_id).find('.muc_media_conference');
	    var notification_area = $(room_id).find('.muc_notification_area');
	    var container = $(room_id).find('.muc_messages_container');
	    
	    room_subjects[room] = subject.text();

	    if (room_subjects[room]) {
		// Prevent users from making multiple confereces
		// if one is already in progress for given room
		button.addClass('ui-disabled');
		notification_area.append('<div class="muc_not_text">Media conference is in progress.</div>' +
					 '<div class="muc_not_btn"><button>Join</button></div>');
		notification_area.enhanceWithin();

		notification_area.find('button').on('click', function() {
		    xc.back_stack.push(xc.getPageID());
		    xc.Utils.navigateToPage('#muc_media_conference');
		    xc.muWebRTC.joinConferenceRoom(room_subjects[room]);
		    registerHangUpConference(room);
		});
		
		// Height of main message container must be recalculated to compensate
		// for notification message
		xc.adjustMessageContainerHeight(room_id);

		if ('#' + xc.getPageID() === room_id) {
		    container.scrollTop(container.prop('scrollHeight'));
		}
	    } else {
		// Enable user conferences
		button.removeClass('ui-disabled');
		notification_area.empty();
		xc.adjustMessageContainerHeight(room_id);

		if ('#' + xc.getPageID() === room_id) {
		    container.scrollTop(container.prop('scrollHeight'));
		}
	    }
	}

	return true;
    }

    function onRoomPresence(presence) {
	console.info('Room presence', presence);
	// If affiliation equals owner display settings button on the room page,
	// otherwise hide it
	var affiliation = $(presence).find('item').attr('affiliation');
	var room_jid = Strophe.getBareJidFromJid($(presence).attr('from'));

	if (affiliation) {
	    xc.muc_affiliations[room_jid] = affiliation;
	}
	
	return true;
    }

    function onRoomRoster(roster) {
	return true;
    }

    
    // Once bookmarks are set user will join all his rooms automatically
    xc.joinRoom = function(room, nickname) {
	xc.connection.muc.join(room, nickname, onRoomMessage, onRoomPresence, onRoomRoster);
    };

    xc.sendRoomDataMessage = function(room, type, payload) {
	var msg = $msg({to: room, type: 'groupchat'}).c('data', {
	    xmlns: 'iotoolchat:muwebrtc:group',
	    type: type
	}).t(payload);

	xc.connection.send(msg);
    };

    xc.changeSubject = function(room, subject) {
	var msg = $msg({to: room, type: 'groupchat'}).c('subject').t(subject);
	xc.connection.send(msg);
    };

    xc.createMediaConferenceNotification = function(room, conference_room) {
	xc.changeSubject(room, conference_room);
    };

    xc.adjustMessageContainerHeight = function(room_id) {
	var window_h = $(window).height();
	var header = $(room_id).find('.muc_header').outerHeight();
	var notification = $(room_id).find('.muc_notification_area').outerHeight();
	var input = $(room_id).find('.muc_textarea').outerHeight();

	var new_height = window_h - header - notification - input;

	$(room_id).find('.muc_messages_container').height(new_height);
    };

    return xc;
})(XmppChat || {});
