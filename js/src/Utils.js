XmppChat.Utils = (function(xc) {
    xc.generateRandomString = function(length) {
	var CHAR_TABLE = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                          'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k',
                          'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'w',
                          'x', 'y', 'z'];

        var random_string = '';

        for (var i = 0; i < length; i++) {
            var random_index = Math.floor(Math.random() * CHAR_TABLE.length);

	    // always try to generate id starting with letter
	    if (random_index < 10 && i === 0) {
		console.log('First character is a number');
		random_index = Math.floor(Math.random() * (CHAR_TABLE.length - 10));
		random_index += 10;
		console.log(random_index);
	    }
	    
            random_string += CHAR_TABLE[random_index];
        }

        return random_string;
    };

    // Check for permissions
    xc.checkWebRTCPermissions = function() {

	function gotPermissions() {
	    var mic = DetectRTC.hasMicrophone;
	    var webcam = DetectRTC.hasWebcam;
	    var w_perm = DetectRTC.isWebsiteHasWebcamPermissions;
	    var m_perm = DetectRTC.isWebsiteHasMicrophonePermissions;
	    var display_warn = false;

	    var warn = '';

	    if (mic && !m_perm) {
		warn += 'You have a microphone but acces to it is denied.';
		display_warn = true;
	    }

	    if (webcam && !w_perm) {
		warn += ' You have a camera device but acces to it is denied.';
		display_warn = true;
	    }

	    if (display_warn) {
		warn += ' To fix this issue please allow access to avaliable devices' +
		    ' otherwise some features of the application will not work.' +
		    ' Please refer to your browser\'s instrunctions on how to enable permissions' +
		    ' for media devices.';
		
		xc.Notification.persistentWarning.set(warn);
	    }
	}

	navigator.mediaDevices.getUserMedia({audio: true, video: true})
	    .then(function(media_stream) {
		var video_tracks = media_stream.getVideoTracks();
		var audio_tracks = media_stream.getAudioTracks();

		var tracks = _.concat(video_tracks, audio_tracks);
		_.forEach(tracks, function(t) {
		    t.stop();
		});
		
		DetectRTC.load(gotPermissions);
	    })
	    .catch(function(err) {
		DetectRTC.load(gotPermissions);
		console.log(err);
	    });
    };

    xc.jidToId = function(jid) {
        var bare_jid = Strophe.getBareJidFromJid(jid);
        return bare_jid.replace(/\.|@/g, '-');
    };

    xc.getTime = function() {
	return Date.now();
    };
    
    xc.getFormatedTime = function(time) {
	var d = new Date(time);
	var hours = d.getHours() > 10 ? d.getHours() : '0' + d.getHours();
	var minutes = d.getMinutes() > 10 ? d.getMinutes() : '0' + d.getMinutes();
	var seconds = d.getSeconds() > 10 ? d.getSeconds() : '0' + d.getSeconds();
	
	return d.getUTCFullYear() + '-' +
	    (d.getMonth() + 1) + '-' +
	    d.getDate() + ' ' +
	    hours + ':' +
	    minutes + ':' +
	    seconds;
    };

    xc.appendDeltaToQuillFile = function(file, delta, cb) {
	console.log(file, delta);
	xc.getFileContent(file, function(content) {
	    var local_delta = JSON.parse(content);

	    // Create empty quill file if it does not exist yet
	    if (typeof local_delta.ops === 'undefined') {
		local_delta.ops = [];
	    }
	    
	    local_delta.ops.push(delta);
	    local_delta = JSON.stringify(local_delta);

	    var blob = new Blob([local_delta], {type: 'application/json'});
	    xc.File.writeFile(file, blob, function() {
		cb();
	    });
	});
    };

    xc.addTimestampToQuillFile = function(file, call_type, call_status, duration, cb) {
	var time = xc.Utils.getTime();
	var duration_string = '';

	if (duration) {
	    duration_string = ' (Duration: ' + duration + 's)';
	}
	
	var insert_string = '\n### DO NOT EDIT THE LINE BELOW ###\n' + call_type + ' call ' + call_status + ': ' +
	    'timestamp: ' + time + duration_string + '\n';
	
	// During file loads we will overwrite timestamp text to ensure that it is
	// inserted correctly
	xc.Utils.appendDeltaToQuillFile(file, {
	    'insert': insert_string,
	    'attributes': {custom: 1234}
	}, cb);
    };

    xc.endVideoCall = function(send_message, video, jid) {
	xc.video_call = false;
	
        xc.WebRTC.endCall(send_message, true, jid);
	xc.UI.resetIncomingVideoCall();

	// remove any leftover hangup buttons
	$('#editor_video_control').empty();

	// Prevent going back if user is editing notes
	if (xc.user_is_editing) {
	    // Pop last item from history since we don't want
	    // for user to navigate back to screen that is not
	    // in use anymore
	    xc.back_stack.pop();
	} else {
	    xc.goBackInStack();
	}
	
	xc.sendSetPresence();
	xc.UI.editButtons.hide();
    };
    
    xc.userConnected = function() {
	if (xc.connection) {
	    if (xc.connection.connected) {
		return true;
	    } else {
		return false;
	    }
	}

	return false;
    };

    xc.changePage = function(page_id, disable_transition) {


	// Due to some unknown reasons transitions with pagecontainer
	// from video call back to previous page does not work correctly
	// temporary workaround for this issue is to disable animations
	// when transitioning from video call page
	$.mobile.navigate(page_id);

	/*
	if (disable_transition) {
	    $.mobile.navigate(page_id);
	} else {
	    $(':mobile-pagecontainer').pagecontainer('change', $(page_id), {
		changeHash: false,
		transition: transition
	    });
	}
	*/

    };

    // Navigate to specified page and fire a callback once
    // page has loaded
    xc.navigateToPage = function(page, callback) {
	$(':mobile-pagecontainer').pagecontainer({
	    change: function(event, ui) {
		if (ui.options.target === page) {
		    if (callback) {
			callback();
		    }
		}
	    }
	});

	$(':mobile-pagecontainer').pagecontainer('change', page);
    };

    xc.editorSaveFile = function(file, cb) {
	// Cycle through all deltas and find ones that have property of
	// timestamp
	var contents = xc.editor.getContents();
	var blob = new Blob([JSON.stringify(contents)], {type: 'application/json'});
	xc.File.writeFile(file, blob, function() {
	    cb();
	});
    };

    /**
     * Load user's data in to text editor.
     * @function loadUsersLog
     * @memberof XmppChat.Utils
     * @param {object} editor Quill.js editor object.
     * @param {string} id Unique id used to select log file name.
     * @param {string} title Tilte of file in editing.
     */
    xc.loadUsersLog = function(editor, id, title) {
	// store file in top level variable to allow
	// saving of data
	xc.editor_file = 'IoToolChat/' + id + '.note';
	
	// Delete any present text from the editor
	xc.File.getFileContent(xc.editor_file, function(content) {
	    if (content === '{}') {
		editor.setContents([]);
	    } else {
		editor.setContents(JSON.parse(content));
	    }
	});

	// Set log title
	var editor_title = document.getElementById('editor_log_name');
	editor_title.innerHTML = title + ' - Log';
    };

    
    /**
     * Retrive JSON Web Token from specified server url
     * @function getJwtToken
     * @memberof XmppChat.Utils
     * @param {string} url Server address.
     * @param {string} username Username to issue token to.
     * @param {string} password Password of the account.
     * @param {function} gotJWTToken Function called with
     * token as a parameter when token is retrieved.
     * @param {function} fail Function called on failed request.
     */
    xc.getJwtToken = function(url, username, password, gotJWT, fail) {
	var xhr = new XMLHttpRequest();

	xhr.onreadystatechange = function() {
	    if (xhr.readyState === 4 && xhr.status === 200) {
		gotJWT(xhr.responseText);
	    } else if (xhr.status === 401) {
		fail();
	    }
	};
	
	xhr.open('POST', url, true);
	xhr.send(JSON.stringify({
	    username: username,
	    password: password
	}));
    };

    /**
     * Get TURN server configuration with credentials from 
     * authorization server
     * @function getTurnConfiguration
     * @memberof XmppChat.Utils
     * @param {string} url Url of the server.
     * @param {string} username Username to issue token to.
     * @param {string} password Password of the account.
     * @param @function gotConfig Function called with configuration
     * as a parameter on successful request.
     * @param {function} fail Function called on failed request.
     */
    xc.getTurnConfiguration = function(token, url, username, gotConfig, fail) {
	var xhr = new XMLHttpRequest();

	xhr.onreadystatechange = function() {
	    if (xhr.readyState === 4 && xhr.status === 200) {
		gotConfig(xhr.responseText);
	    } else if (xhr.status === 401) {
		fail();
	    }	    
	};

	xhr.open('POST', url, true);
	xhr.setRequestHeader('Authorization', 'Bearer ' + token);
	xhr.send(JSON.stringify({username: username}));
    };

    xc.checkConnection = function(cb) {
	var xhr = new XMLHttpRequest();
	var file = 'https://turnserver.iotool.io/img/chat_ui_elements/options.png';
	var random_num = Math.round(Math.random() * 10000);

	xhr.onreadystatechange = function() {
	    if (xhr.readyState === 4) {
		if (xhr.status >= 200 && xhr.status < 304) {
		    // Server is reachable
		    cb(true);
		} else if (this.status === 0) {
		    // Server is unreachable
		    cb(false);
		}
	    }
	};

	xhr.open('HEAD', file + '?rand=' + random_num, true);

	xhr.send();
    };

    return xc;
}(XmppChat || {}));
